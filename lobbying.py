#
# Utilities for analyzing the lobbying disclosure data available at:
#
#   https://www.senate.gov/legislative/Public_Disclosure/database_download.htm
#
# An example of an XML 'contributions' filing is:
#
#   {
#    "@ID": "A8D2CFE9-C32C-4EED-906E-6A6D39BDE05C",
#    "@Year": "2019",
#    "@Received": "2020-01-01T08:32:13.107",
#    "@Type": "LD-203 YEAR-END REPORT",
#    "@Period": "Year-End (July 1 - Dec 31)",
#    "Registrant": {
#     "@xmlns": "",
#     "@RegistrantID": "54302",
#     "@RegistrantName": "AMERICAN FED OF GOVERNMENT EMPLOYEES, AFL-CIO",
#     "@Address": "80 F STREET, NW\r\nWASHINGTON, DC 20001",
#     "@RegistrantCountry": "USA"
#    },
#    "Lobbyist": {
#     "@xmlns": "",
#     "@LobbyistName": "SNYDER, TIMOTHY"
#    },
#    "Contributions": {
#     "Contribution": [
#      {
#       "@xmlns": "",
#       "@Contributor": "SNYDER, TIMOTHY",
#       "@ContributionType": "FECA",
#       "@Payee": "Act Blue",
#       "@Honoree": "Amy McGrath",
#       "@Amount": "5.0000",
#       "@ContributionDate": "2019-10-09T00:00:00"
#      },
#      ...
#     ]
#    }
#   }
#

import csv
import glob
import json
import pandas as pd
import xmltodict


# Remove leading and trailing whitespace then merge whitespaces.
def simplify_string(value):
    return ' '.join(value.strip().split())


def convert_xml_to_json(xml_file, json_file):
    '''Converts an XML file of OPR lobbying disclosures into a JSON file.

    Args:
      xml_file: The input XML file.
      json_file: The output JSON file.
    '''
    json_results = xmltodict.parse(open(xml_file, encoding='utf-16').read())
    with open(json_file, 'w') as outfile:
        json.dump(json_results, outfile, indent=1)


def convert_xml_files_to_json(xml_pattern):
    '''Converts a set of XML files into JSON using a regular expression glob.

    Each output JSON filename is chosen by replacing the '.xml' extension of
    each input file with '.json'.

    Args:
      xml_pattern: The regular expression pattern to glob XML files with.
    '''
    xml_files = sorted(glob.glob(xml_pattern))
    print('Will convert the following to JSON: {}'.format(xml_files))
    for xml_file in xml_files:
        # Remove the '.xml' from the filename and append '.json'
        json_file = '{}.json'.format(xml_file[:-4])

        print('Converting {} to {}'.format(xml_file, json_file))
        convert_xml_to_json(xml_file, json_file)


def get_issues_year(filing):
    '''Returns the year the lobbying filing corresponds to.'''
    if not '@Year' in filing:
        return None
    return filing['@Year']


def get_issues_datetime_received(filing):
    '''Returns the date and time the report was received.'''
    if not '@Received' in filing:
        return None
    return filing['@Received']


def get_issues_rough_amount(filing):
    '''Returns rough amount of USD spent in the activity in the JSON filing.'''
    if not '@Amount' in filing:
        return None
    return filing['@Amount']


def get_issues_type(filing):
    '''Returns the type of filing report of the JSON filing.

    This seems to encode which quarter the report is for, e.g.,
    "FOURTH QUARTER REPORT".
    '''
    if not '@Type' in filing:
        return None
    return filing['@Type']


def get_issues_period(filing):
    '''Returns the time period of the JSON lobbying filing.'''
    if not '@Period' in filing:
        return None
    return filing['@Period']


def get_issues_affiliated_orgs_url(filing):
    '''Returns the affiliated organizations URL -- if any -- of the filing.'''
    if not '@AffiliatedOrgsURL' in filing:
        return None
    return filing['@AffiliatedOrgsURL']


def get_issues_affiliated_orgs(filing):
    '''Returns the list of affiliated organizations from the filing.'''
    if not 'AffiliatedOrgs' in filing:
        return []
    affiliated_orgs = filing['AffiliatedOrgs']
    if not 'Org' in affiliated_orgs:
        return []
    orgs = affiliated_orgs['Org']
    affiliated_list = []
    if type(orgs) is dict:
        if '@AffiliatedOrgName' in orgs:
            affiliated_list.append(orgs['@AffiliatedOrgName'])
    else:
        for org in orgs:
            if '@AffiliatedOrgName' in org:
                affiliated_list.append(org['@AffiliatedOrgName'])
    return affiliated_list


def get_issues_registrant(filing):
    '''Returns the registrant object for a JSON lobbying filing.'''
    if 'Registrant' in filing:
        return filing['Registrant']
    else:
        return None


def get_issues_registrant_name(filing):
    '''Returns the registrant name string for a JSON lobbying filing.'''
    registrant = get_issues_registrant(filing)
    if registrant is None:
        return None

    if '@RegistrantName' in registrant:
        return registrant['@RegistrantName']
    else:
        return None


def get_issues_registrant_description(filing):
    '''Returns general description of registrant for JSON lobbying filing.'''
    registrant = get_issues_registrant(filing)
    if registrant is None:
        return None

    if '@GeneralDescription' in registrant:
        return registrant['@GeneralDescription']
    else:
        return None


def get_issues_registrant_address(filing):
    '''Returns the registrant address string for a JSON lobbying filing.'''
    registrant = get_issues_registrant(filing)
    if registrant is None:
        return None

    if '@Address' in registrant:
        return registrant['@Address']
    else:
        return None


def get_issues_registrant_country(filing):
    '''Returns the registrant country string for a JSON lobbying filing.'''
    registrant = get_issues_registrant(filing)
    if registrant is None:
        return None

    if '@RegistrantCountry' in registrant:
        return registrant['@RegistrantCountry']
    else:
        return None


def get_issues_client(filing):
    '''Returns the client object for a JSON lobbying filing.'''
    if 'Client' in filing:
        return filing['Client']
    else:
        return None


def get_issues_client_name(filing):
    '''Returns the client name for a JSON lobbying filing.'''
    client = get_issues_client(filing)
    if client is None:
        return None

    if '@ClientName' in client:
        return client['@ClientName']
    else:
        return None


def get_issues_client_description(filing):
    '''Returns general description of client for the JSON lobbying filing.'''
    client = get_issues_client(filing)
    if client is None:
        return None

    if '@GeneralDescription' in client:
        return client['@GeneralDescription']
    else:
        return None


def get_issues_client_contact(filing):
    '''Returns the client contact name for a JSON lobbying filing.'''
    client = get_issues_client(filing)
    if client is None:
        return None

    if '@ContactFullname' in client:
        return client['@ContactFullname']
    else:
        return None


def get_issues_client_state(filing):
    '''Returns the client's state for a JSON lobbying filing.'''
    client = get_issues_client(filing)
    if client is None:
        return None

    if '@ClientState' in client:
        return client['@ClientState']
    else:
        return None


def get_issues_client_country(filing):
    '''Returns the client's country for a JSON lobbying filing.'''
    client = get_issues_client(filing)
    if client is None:
        return None

    if '@ClientCountry' in client:
        return client['@ClientCountry']
    else:
        return None


def get_issues_lobbyists(filing):
    '''Returns the list of lobbyist objects for a JSON lobbying filing.'''
    if not 'Lobbyists' in filing:
        return None
    lobbyists = filing['Lobbyists']
    if not 'Lobbyist' in lobbyists:
        return None

    # If there is only one lobbyist, the result is a single dictionary, rather
    # than an array of length one. We perform the conversion into an array in
    # this case for the sake of consistency.
    lobbyist_obj = lobbyists['Lobbyist']
    if type(lobbyist_obj) is dict:
        return [
            lobbyist_obj,
        ]
    else:
        return lobbyist_obj


def get_issues_lobbyist_names(filing):
    '''Returns the list of lobbyist names for a JSON lobbying filing.'''
    lobbyists = get_issues_lobbyists(filing)
    if lobbyists is None:
        return []

    names = []
    for lobbyist in lobbyists:
        if '@LobbyistName' in lobbyist:
            name = lobbyist['@LobbyistName']
            names.append(name)
        else:
            names.append('')

    return names


def get_issues_lobbyist_positions(filing):
    '''Returns the list of lobbyist positions for a JSON lobbying filing.'''
    lobbyists = get_issues_lobbyists(filing)
    if lobbyists is None:
        return []

    positions = []
    for lobbyist in lobbyists:
        if '@OfficialPosition' in lobbyist:
            positions.append(lobbyist['@OfficialPosition'])
        else:
            positions.append('')

    return positions


def get_issues_government_entities(filing):
    '''Returns the list of government entity objects in a JSON filing.'''
    if not 'GovernmentEntities' in filing:
        return None
    government_entities = filing['GovernmentEntities']
    if not 'GovernmentEntity' in government_entities:
        return None

    # If there is only one entity, the result is a single dictionary, rather
    # than an array of length one. We perform the conversion into an array in
    # this case for the sake of consistency.
    entity_obj = government_entities['GovernmentEntity']
    if type(entity_obj) is dict:
        return [
            entity_obj,
        ]
    else:
        return entity_obj


def get_issues_government_entity_names(filing):
    '''Returns the list of government entities lobbied in a JSON filing.'''
    entities = get_issues_government_entities(filing)
    if entities is None:
        return []

    names = []
    for entity in entities:
        if '@GovEntityName' in entity:
            names.append(entity['@GovEntityName'])

    return sorted(names)


def get_issues_issues(filing):
    '''Returns the list of issue objects in a JSON filing.'''
    if not 'Issues' in filing:
        return None
    issues = filing['Issues']
    if not 'Issue' in issues:
        return None

    # If there is only one code, the result is a single dictionary, rather than
    # an array of length one. We perform the conversion into an array in this
    # case for the sake of consistency.
    issue_obj = issues['Issue']
    if type(issue_obj) is dict:
        return [
            issue_obj,
        ]
    else:
        return issue_obj


def get_issues_issue_codes(filing):
    '''Returns the list of issue description codes in a JSON filing.'''
    issues = get_issues_issues(filing)
    if issues is None:
        return []

    codes = []
    for issue in issues:
        if '@Code' in issue:
            codes.append(issue['@Code'])
        else:
            codes.append('')

    return codes


def get_issues_specific_issues(filing):
    '''Returns the list of specific issues lobbied for in a JSON filing.'''
    issues = get_issues_issues(filing)
    if issues is None:
        return []

    specifics = []
    for issue in issues:
        if '@SpecificIssue' in issue:
            specifics.append(issue['@SpecificIssue'])
        else:
            specifics.append('')

    return specifics


def get_issues_conviction_disclosure(filing):
    '''Returns the conviction disclosure (if any) in a JSON lobbying filing.'''
    if not 'ConvictionDisclosure' in filing:
        return None
    disclosure = filing['ConvictionDisclosure']
    if not 'Conviction' in disclosure:
        return None
    conviction = disclosure['Conviction']
    if not '@ConvictionReported' in conviction:
        return None
    return conviction['@ConvictionReported']


# TODO(Jack Poulson): Get @ConvictedLobbyists results.


def get_json_filings(json_pattern):
    '''Returns array of JSON filings in files matching the given regexp.'''
    filings = []
    json_files = sorted(glob.glob(json_pattern))
    for json_file in json_files:
        json_data = json.load(open(json_file))
        if json_data['PublicFilings'] is None:
          continue
        filing_obj = json_data['PublicFilings']['Filing']
        if type(filing_obj) is dict:
            # Properly store the singleton.
            filing_obj = [filing_obj]
        filings += filing_obj

    return filings


def simplify_issues_filings(filings):
    '''Returns flattened JSON array object for the list of lobbying filings.'''
    simple_filings = []
    for filing in filings:
        simple_filing = {}
        simple_filing['year'] = get_issues_year(filing)
        simple_filing['datetime_received'] = get_issues_datetime_received(
            filing)
        simple_filing['amount'] = get_issues_rough_amount(filing)
        simple_filing['type'] = get_issues_type(filing)
        simple_filing['period'] = get_issues_period(filing)
        simple_filing['affiliated_orgs_url'] = get_issues_affiliated_orgs_url(
            filing)
        simple_filing['registrant_name'] = get_issues_registrant_name(filing)
        simple_filing[
            'registrant_description'] = get_issues_registrant_description(
                filing)
        simple_filing['registrant_address'] = get_issues_registrant_address(
            filing)
        simple_filing['registrant_country'] = get_issues_registrant_country(
            filing)
        simple_filing['client_name'] = get_issues_client_name(filing)
        simple_filing['client_description'] = get_issues_client_description(
            filing)
        simple_filing['client_contact'] = get_issues_client_contact(filing)
        simple_filing['client_state'] = get_issues_client_state(filing)
        simple_filing['client_country'] = get_issues_client_country(filing)
        simple_filing['lobbyist_names'] = json.dumps(
            get_issues_lobbyist_names(filing))
        simple_filing['lobbyist_names_lower'] = (
            simple_filing['lobbyist_names'].lower())
        simple_filing['lobbyist_positions'] = json.dumps(
            get_issues_lobbyist_positions(filing))
        simple_filing['government_entities'] = json.dumps(
            get_issues_government_entity_names(filing))
        simple_filing['affiliated_orgs'] = json.dumps(
            get_issues_affiliated_orgs(filing))
        simple_filing['affiliated_orgs_lower'] = (
            simple_filing['affiliated_orgs'].lower())
        simple_filing['issue_codes'] = json.dumps(
            get_issues_issue_codes(filing))
        simple_filing['specific_issues'] = json.dumps(
            get_issues_specific_issues(filing))
        simple_filing[
            'conviction_disclosure'] = get_issues_conviction_disclosure(filing)
        simple_filings.append(simple_filing)

    return simple_filings


def convert_simple_contributions_filings_json_to_csv(simple_filings,
                                                     csv_filings):
    '''Converts JSON file of simple contributions filings into a CSV.'''
    dtype = {'registrant_id': str}
    df = pd.read_json(simple_filings, dtype=dtype)

    # We must use a tab separator and avoid newlines or MySQL will truncate
    # all text inputs to 255 characters. This causes JSON parsing issues for
    # some of Google's "lobbyist_positions" fields.

    df = df.replace(r'\\r\\n', '; ', regex=True)
    df = df.replace(r'\\r', '; ', regex=True)
    df = df.replace(r'\\n', '; ', regex=True)

    df = df.replace(r'\r\n', '; ', regex=True)
    df = df.replace(r'\r', '; ', regex=True)
    df = df.replace(r'\n', '; ', regex=True)

    df = df.replace(r'\\', r'\\\\', regex=True)

    # Replace all sequences of the form '\...;' with ';' since PostgreSQL
    # refuses to parse JSON otherwise.
    df = df.replace(r'\\+;', r';', regex=True)

    df.datetime_received = pd.to_datetime(df.datetime_received,
                                          errors='coerce')

    duplicated = df[[
        'year', 'datetime_received', 'period', 'registrant_name',
        'registrant_country', 'lobbyist_name', 'contribution'
    ]].duplicated(keep='first')
    num_dropped = sum(duplicated)
    print('Dropping {} redundant rows.'.format(num_dropped))
    df = df.drop(df[duplicated].index)

    df.to_csv(csv_filings,
              index=False,
              escapechar='\\',
              quotechar='"',
              doublequote=False,
              sep='\t',
              quoting=csv.QUOTE_ALL)


def convert_simple_issues_filings_json_to_csv(simple_filings, csv_filings):
    '''Converts JSON file of simple issues filings into a CSV.'''
    df = pd.read_json(simple_filings)

    # We must use a tab separator and avoid newlines or MySQL will truncate
    # all text inputs to 255 characters. This causes JSON parsing issues for
    # some of Google's "lobbyist_positions" fields.
    df = df.replace(r'\\r\\n', '; ', regex=True)
    df = df.replace(r'\\r', '; ', regex=True)
    df = df.replace(r'\\n', '; ', regex=True)

    df = df.replace(r'\r\n', '; ', regex=True)
    df = df.replace(r'\r', '; ', regex=True)
    df = df.replace(r'\n', '; ', regex=True)

    df = df.replace(r'\\', r'\\\\', regex=True)

    # Replace all sequences of the form '\...;' with ';' since PostgreSQL
    # refuses to parse JSON otherwise.
    df = df.replace(r'\\+;', r';', regex=True)

    df.datetime_received = pd.to_datetime(df.datetime_received,
                                          errors='coerce')

    duplicated = df[[
        'year', 'datetime_received', 'type', 'period', 'registrant_name',
        'client_name', 'client_state', 'client_country'
    ]].duplicated(keep='first')
    num_dropped = sum(duplicated)
    print('Dropping {} redundant rows.'.format(num_dropped))
    df = df.drop(df[duplicated].index)

    df.to_csv(csv_filings,
              index=False,
              escapechar='\\',
              quotechar='"',
              doublequote=False,
              sep='\t',
              quoting=csv.QUOTE_ALL)


def create_csv_from_issues_xml(base_pattern, basename='issues'):
    '''Creates a CSV of simplified issues filings in the given base glob.'''
    convert_xml_files_to_json('{}.xml'.format(base_pattern))
    filings = get_json_filings('{}.json'.format(base_pattern))
    simplified_filings = simplify_issues_filings(filings)
    with open('{}.json'.format(basename), 'w') as outfile:
        json.dump(simplified_filings, outfile, indent=1)
    convert_simple_issues_filings_json_to_csv('{}.json'.format(basename),
                                              '{}.csv'.format(basename))


def get_contributions_year(filing):
    '''Returns the year of a contribution filing.'''
    if not '@Year' in filing:
        return None
    return filing['@Year']


def get_contributions_datetime_received(filing):
    '''Returns the date-time that the contribution filing was received.'''
    if not '@Received' in filing:
        return None
    return filing['@Received']


def get_contributions_type(filing):
    '''Returns the type of the contribution filing.'''
    if not '@Type' in filing:
        return None
    return filing['@Type']


def get_contributions_period(filing):
    '''Returns the period of the contribution filing.'''
    if not '@Period' in filing:
        return None
    return filing['@Period']


def get_contributions_registrant(filing):
    '''Returns the registrant object for the contribution filing.'''
    if not 'Registrant' in filing:
        return None
    return filing['Registrant']


def get_contributions_id(filing):
    '''Returns the ID for the contribution filing.'''
    if not '@ID' in filing:
        return None
    return filing['@ID']


def get_contributions_comments(filing):
    '''Returns the comments for the contribution filing.'''
    if not '@Comments' in filing:
        return None
    return filing['@Comments']


def get_contributions_registrant_id(filing):
    '''Returns the registrant ID for the contribution filing.'''
    registrant = get_contributions_registrant(filing)
    if registrant is None:
        return None

    if not '@RegistrantID' in registrant:
        return None
    return registrant['@RegistrantID']


def get_contributions_registrant_name(filing):
    '''Returns the registrant name for the contribution filing.'''
    registrant = get_contributions_registrant(filing)
    if registrant is None:
        return None

    if not '@RegistrantName' in registrant:
        return None
    return registrant['@RegistrantName']


def get_contributions_registrant_address(filing):
    '''Returns the registrant address for the contribution filing.'''
    registrant = get_contributions_registrant(filing)
    if registrant is None:
        return None

    if not '@Address' in registrant:
        return None
    return registrant['@Address']


def get_contributions_registrant_country(filing):
    '''Returns the registrant country for the contribution filing.'''
    registrant = get_contributions_registrant(filing)
    if registrant is None:
        return None

    if not '@RegistrantCountry' in registrant:
        return None
    return registrant['@RegistrantCountry']


def get_contributions_lobbyist(filing):
    '''Returns the lobbyist object for the contribution filing.'''
    if not 'Lobbyist' in filing:
        return None
    return filing['Lobbyist']


def get_contributions_lobbyist_name(filing):
    '''Returns the lobbyist name for the contribution filing.'''
    lobbyist = get_contributions_lobbyist(filing)
    if lobbyist is None:
        return None

    if not '@LobbyistName' in lobbyist:
        return None
    return lobbyist['@LobbyistName']


def get_contributions_contributions(filing):
    '''Returns the list of contribution objects for the filing.'''
    if not 'Contributions' in filing:
        return None
    contributions_parent = filing['Contributions']
    if not 'Contribution' in contributions_parent:
        return None
    return contributions_parent['Contribution']


def simplify_contributions_filings(filings):
    '''Returns flattened JSON array object for list of contribution filings.'''
    simple_filings = []
    for filing in filings:
        header = {}
        header['contribution_id'] = get_contributions_id(filing)
        header['year'] = get_contributions_year(filing)
        header['datetime_received'] = get_contributions_datetime_received(
            filing)
        header['type'] = get_contributions_type(filing)
        header['period'] = get_contributions_period(filing)
        header['comments'] = get_contributions_comments(filing)
        header['registrant_id'] = get_contributions_registrant_id(filing)
        header['registrant_name'] = get_contributions_registrant_name(filing)
        header['registrant_address'] = get_contributions_registrant_address(
            filing)
        header['registrant_country'] = get_contributions_registrant_country(
            filing)
        header['lobbyist_name'] = get_contributions_lobbyist_name(filing)

        contributions = get_contributions_contributions(filing)
        if contributions is not None:
            if type(contributions) is dict:
                contributions = [contributions]
            for contribution in contributions:
                contribution_new = {}
                if '@Contributor' in contribution:
                    contribution_new['contributor'] = contribution[
                        '@Contributor']
                if '@ContributionType' in contribution:
                    contribution_new['type'] = contribution[
                        '@ContributionType']
                if '@Payee' in contribution:
                    contribution_new['payee'] = contribution['@Payee']
                if '@Honoree' in contribution:
                    contribution_new['honoree'] = contribution[
                        '@Honoree']
                if '@Amount' in contribution:
                    contribution_new['amount'] = contribution['@Amount']
                if '@ContributionDate' in contribution:
                    contribution_new['date'] = contribution[
                        '@ContributionDate']
                simple_filing = header.copy()
                simple_filing['contribution'] = json.dumps(contribution_new)
                simple_filings.append(simple_filing)
        else:
            simple_filing = header.copy()
            simple_filing['contribution'] = None
            simple_filings.append(simple_filing)

    return simple_filings


def create_csv_from_contributions_xml(base_pattern, basename='contributions'):
    '''Creates a CSV of simplified contribution filings in given base glob.'''
    convert_xml_files_to_json('{}.xml'.format(base_pattern))
    filings = get_json_filings('{}.json'.format(base_pattern))
    simplified_filings = simplify_contributions_filings(filings)
    with open('{}.json'.format(basename), 'w') as outfile:
        json.dump(simplified_filings, outfile, indent=1)
    convert_simple_contributions_filings_json_to_csv(
        '{}.json'.format(basename), '{}.csv'.format(basename))


def get_contributions_registrant_name_list(contributions):
    registrants_set = set()
    for contribution in contributions:
        registrant = contribution['registrant_name']
        if registrant is not None:
            registrants_set.add(simplify_string(registrant))
    return sorted(list(registrants_set))


def get_contributions_lobbyist_name_list(contributions):
    lobbyists_set = set()
    for contribution in contributions:
        lobbyist = contribution['lobbyist_name']
        if lobbyist is not None:
            lobbyists_set.add(simplify_string(lobbyist))
    return sorted(list(lobbyists_set))


def get_contributions_contributor_list(contributions):
    contributors_set = set()
    for contribution in contributions:
        subcontributions = contribution['contributions']
        if subcontributions is None:
            continue
        subcontributions = json.loads(subcontributions)
        for subcontribution in subcontributions:
            contributor = subcontribution['contributor']
            if contributor is not None:
                contributors_set.add(simplify_string(contributor))
    return sorted(list(contributors_set))


def get_contributions_honoree_list(contributions):
    honorees_set = set()
    for contribution in contributions:
        subcontributions = contribution['contributions']
        if subcontributions is None:
            continue
        subcontributions = json.loads(subcontributions)
        for subcontribution in subcontributions:
            honoree = subcontribution['honoree']
            if honoree is not None:
                honorees_set.add(simplify_string(honoree))
    return sorted(list(honorees_set))


def get_contributions_payee_list(contributions):
    payees_set = set()
    for contribution in contributions:
        subcontributions = contribution['contributions']
        if subcontributions is None:
            continue
        subcontributions = json.loads(subcontributions)
        for subcontribution in subcontributions:
            payee = subcontribution['payee']
            if payee is not None:
                payees_set.add(simplify_string(payee))
    return sorted(list(payees_set))


def get_issues_registrant_name_list(issues):
    names_set = set()
    for issue in issues:
        name = issue['registrant_name']
        if name is not None:
            names_set.add(simplify_string(name))
    return sorted(list(names_set))


def get_issues_client_name_list(issues):
    names_set = set()
    for issue in issues:
        name = issue['client_name']
        if name is not None:
            names_set.add(simplify_string(name))
    return sorted(list(names_set))


def get_issues_affiliated_orgs_list(issues):
    names_set = set()
    for issue in issues:
        orgs = issue['affiliated_orgs']
        if orgs is None:
            continue
        orgs = json.loads(orgs)
        for org in orgs:
            names_set.add(simplify_string(org))
    return sorted(list(names_set))


def get_issues_clients_with_convictions(issues):
    client_set = set()
    for issue in issues:
        convicted = issue['conviction_disclosure']
        if convicted is None or convicted == '' or convicted == 'no':
            continue
        client = issue['client_name']
        client_set.add(simplify_string(client))
    return sorted(list(client_set))

